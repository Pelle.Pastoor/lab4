package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private CellState[][] grid;
    private CellState initiaState;

    public CellGrid(int rows, int columns, CellState initialState) {
         
        grid = new CellState[rows][columns];
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return grid.length;
    }

    @Override
    public int numColumns() {
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row < 0 && row >= numRows()){
            throw new IndexOutOfBoundsException("Row is out of bound");
        }else if(column < 0 || column >= numColumns()){
            throw new IndexOutOfBoundsException("Columns it out of bound");
        }

        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if(row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException("Row is out of bound");
        }else if(column < 0 || column >= numColumns()){
            throw new IndexOutOfBoundsException("Columns it out of bound");
        }

        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(numRows(), numColumns(), initiaState);
        for(int row = 0; row < numRows(); row++){
            for(int col = 0; col < numColumns(); col++){
                copyGrid.set(row, col, get(row, col));
            }
        }
        return copyGrid;
    }
    
}
